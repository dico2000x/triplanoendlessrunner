﻿using UnityEngine;

namespace EndlessRunner.Utils
{
    public sealed class LineMovement
    {
        private int _indexLineCurrent;
        private int _lineTotal;
        private readonly int[] _linesPosition = new int[3] {-4, 0, 4};
        public float linePositionCurrent = 0;

        public LineMovement(int indexLine)
        {
            _lineTotal = _linesPosition.Length;
            _SetIndexLine(indexLine);
        }

        public LineMovement()
        {
            _lineTotal = _linesPosition.Length;
            RandomLinePosition();
        }

        public void MoveLeft()
        {
            _SetIndexLine(_indexLineCurrent - 1);
        }

        public void MoveRight()
        {
            _SetIndexLine(_indexLineCurrent + 1);
        }

        public void RandomLinePosition()
        {
            int randomLine = Random.Range(0, _lineTotal);
            _SetIndexLine(randomLine);
        }

        private void _SetIndexLine(int index)
        {
            _indexLineCurrent = index;
            _CheckLineLimit();
            _SetLinePosition();
        }

        private void _CheckLineLimit()
        {
            if(_indexLineCurrent < 0)
            {
                _indexLineCurrent = 0;
            }
            else if(_indexLineCurrent >= _lineTotal)
            {
                _indexLineCurrent = _lineTotal - 1;
            }
        }

        private void _SetLinePosition()
        {
            linePositionCurrent = _linesPosition[_indexLineCurrent];
        }
    }
}
