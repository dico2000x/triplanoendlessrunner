﻿using System.Collections;
using UnityEngine;

namespace EndlessRunner.Utils
{
    public class Fade : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _loading;
        [Range(0.1f, 1)]
        [SerializeField] private float _fadeTime = 0.5f;

        public IEnumerator FadeInCoroutine()
        {
            float start = 0;
            float end = 1;
            float speed = (end - start) / _fadeTime;
            
            _loading.alpha = start;
            while(_loading.alpha < end)
            {
                _loading.alpha += speed * Time.unscaledDeltaTime;
                yield return null;
            }

            _loading.alpha = end;
        }

        public IEnumerator FadeOutCoroutine()
        {
            float start = 1;
            float end = 0;
            float speed = (end - start) / _fadeTime;
            
            _loading.alpha = start;
            while(_loading.alpha > end)
            {
                _loading.alpha += speed * Time.unscaledDeltaTime;
                yield return null;
            }

            _loading.alpha = end;
        }
    }
}