﻿using System.Collections.Generic;
using UnityEngine;
using EndlessRunner.Utils;

namespace EndlessRunner.Spawner.Obstacles 
{
    public sealed class SpawnObstacles : MonoBehaviour
    {
        private LineMovement _lineMovement;
        [SerializeField] private ObstaclePool[] _obstaclesPool;
        private Dictionary<string, List<GameObject>> _obstaclesDictionary = new Dictionary<string, List<GameObject>>();
        [SerializeField] private float _distanceToSpawn = 50;
        private Vector3 _distanceWalking;
        [SerializeField] private float _maxDistanceWalkingToSpawn = 50;
        [SerializeField] private Transform _target;
        [SerializeField] private LayerMask _checkItemLayer;
        private Transform _parent;

        private void Awake() 
        {
            _lineMovement = new LineMovement();
            GameObject parentGameObject = new GameObject("SpawnObstacles");
            _parent = parentGameObject.transform;
            _parent.SetParent(this.transform, false);
        }

        private void Start()
        {
            _InitializePooling();
        }

        private void Update()
        {
            _CheckDistanceToSpawnObstacles();
            _CheckAllObstaclesToDisable();
        }

        private void _InitializePooling()
        {
            foreach(ObstaclePool pool in _obstaclesPool)
            {
                _obstaclesDictionary.Add(pool.tag, new List<GameObject>());
            }
        }

        private void _CheckDistanceToSpawnObstacles()
        {
            Vector3 distance = _target.position - _distanceWalking;
            if(distance.magnitude >= _maxDistanceWalkingToSpawn)
            {
                _distanceWalking = _target.position;
                _SpawnRandomObstacle();
            }
        }

        private void _CheckAllObstaclesToDisable()
        {
            foreach(ObstaclePool pool in _obstaclesPool)
            {
                List<GameObject> obstaclesList = _obstaclesDictionary[pool.tag];

                foreach(GameObject obstacle in obstaclesList)
                {
                    if(obstacle.transform.position.z <= _target.position.z - 10)
                    {
                        obstacle.gameObject.SetActive(false);
                    }
                }
            }
        }

        private void _SpawnRandomObstacle()
        {
            Vector3 spawnPosition = _CalculateSpawnPosition();

            if(_HasObjectInPosition(spawnPosition))
                return;

            ObstaclePool pool = _GetRandomObstaclePool();
            spawnPosition.y = pool.prefab.transform.position.y;

            List<GameObject> obstacles = _GetAllObstaclesDisabled(pool.tag);
            if(obstacles.Count > 0)
            {
                GameObject obstacleDisabled = obstacles[0];
                _ReuseObstacles(obstacleDisabled, spawnPosition);
            }
            else
            {
                _SpawnObstacle(pool, spawnPosition);
            }
        }

        private void _ReuseObstacles(GameObject obstacleDisabled, Vector3 newPosition)
        {
            obstacleDisabled.transform.position = newPosition;
            obstacleDisabled.SetActive(true);
        }

        private List<GameObject> _GetAllObstaclesDisabled(string tag)
        {
            List<GameObject> obstaclesDisabled = new List<GameObject>();
            if(!this._obstaclesDictionary.ContainsKey(tag))
                return obstaclesDisabled;


            List<GameObject> obstacles = _obstaclesDictionary[tag];
            foreach(GameObject obstacle in obstacles)
            {
                if(obstacle.activeInHierarchy)
                    continue;

                obstaclesDisabled.Add(obstacle);
            }

            return obstaclesDisabled;
        }

        private void _SpawnObstacle(ObstaclePool obstacle, Vector3 spawnPosition)
        {
            GameObject prefab = obstacle.prefab;
            GameObject newObstacle = Instantiate(prefab, spawnPosition, prefab.transform.rotation);
            newObstacle.transform.SetParent(_parent, false);
            string tag = obstacle.tag;
            _obstaclesDictionary[tag].Add(newObstacle);
        }

        private ObstaclePool _GetRandomObstaclePool()
        {
            int randomObstacle = _GetRandomObstaclesValue();
            return _obstaclesPool[randomObstacle];
        }

        private int _GetRandomObstaclesValue()
        {
            int random = Random.Range(0, _obstaclesPool.Length);
            return random;
        }

        private Vector3 _CalculateSpawnPosition()
        {
            _lineMovement.RandomLinePosition();
            Vector3 position = new Vector3(_lineMovement.linePositionCurrent, 1, _target.position.z + _distanceToSpawn);
            return position;
        }

        private bool _HasObjectInPosition(Vector3 position)
        {
            Collider[] itens = Physics.OverlapSphere(position, 2, _checkItemLayer);
            return itens.Length > 0;
        }
    }
}