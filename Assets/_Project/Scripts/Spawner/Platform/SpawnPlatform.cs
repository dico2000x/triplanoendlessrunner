﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.Spawner.Platform 
{
    public sealed class SpawnPlatform : MonoBehaviour
    {
        [SerializeField] private GameObject _platformPrefab;
        [SerializeField] private Transform _target;
        [SerializeField] private int _totalPlatform = 5;
        private List<GameObject> _platforms = new List<GameObject>();
        [SerializeField] private float _distanceBetweenPlatforms = 24;
        [SerializeField] private float _depthInitial = 44;
        private Transform _firstPlatform;
        private Transform _lastPlatform;
        private Transform _parent;
        
        private void Awake()
        {
            GameObject parentGameObject = new GameObject("SpawnPlatform");
            _parent = parentGameObject.transform;
            _parent.SetParent(this.transform, false);
        }

        private void Start()
        {
            _InitializePlatforms();
        }

        private void Update()
        {
            if(_CanRepositionFirstPlatform())
            {
                _RepositionFirstPlatformToEnd();
            }
        }

        private void _InitializePlatforms()
        {
            for(int i = 0; i < _totalPlatform; i++)
            {
                Vector3 spawnPosition = new Vector3(0, 0, _depthInitial + _distanceBetweenPlatforms * (i + 1));
                _Spawn(spawnPosition);
            }

            _SetFirstPlatform();
            _SetLastPlatform();
        }

        private void _Spawn(Vector3 spawnPosition)
        {
            GameObject platform = Instantiate(_platformPrefab, spawnPosition, Quaternion.identity);
            platform.transform.SetParent(_parent, false);
            _platforms.Add(platform);
        }

        private bool _CanRepositionFirstPlatform()
        {
            float offsetZ = 5;
            return _target.position.z - offsetZ >= _firstPlatform.position.z;
        }

        private void _RepositionFirstPlatformToEnd()
        {
            _platforms.Remove(_firstPlatform.gameObject);
            _platforms.Add(_firstPlatform.gameObject);

            float lastPlatformPositionZ = _lastPlatform.position.z;
            _firstPlatform.position = new Vector3(0, 0, lastPlatformPositionZ + _distanceBetweenPlatforms);

            _SetFirstPlatform();
            _SetLastPlatform();
        }

        private void _SetFirstPlatform()
        {
            _firstPlatform = _platforms.First().transform;
        }

        private void _SetLastPlatform()
        {
            _lastPlatform = _platforms.Last().transform;
        }
    }
}