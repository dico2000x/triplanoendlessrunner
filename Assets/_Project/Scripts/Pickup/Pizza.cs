﻿using UnityEngine;
using EndlessRunner.Interfaces.Collision;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Pickup
{
    public sealed class Pizza : MonoBehaviour , IPlayerTrigger
    {
        [SerializeField] private IntEvent _OnAddPizza = default;

        void IPlayerTrigger.Interactable()
        {
            gameObject.SetActive(false);
            _AddPizza();
        }

        private void _AddPizza()
        {
            _OnAddPizza.Raise(1);
        }
    }
}