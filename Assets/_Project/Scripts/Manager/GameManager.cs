﻿using UnityEngine;
using EndlessRunner.Utils;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Manager
{
    public sealed class GameManager : MonoBehaviour
    {
        private int _coin = 0;
        [SerializeField] private IntEvent _OnCoinCollect = default;
        [SerializeField] private IntEvent _OnCoinChange = default;


        private int _pizzas = 0;
        [SerializeField] private IntEvent _OnAddPizza = default;
        [SerializeField] private IntEvent _OnRemovePizza = default;
        [SerializeField] private IntEvent _OnPizzaChange = default;


        private int _pizzasDelivered = 0;
        [SerializeField] private GameEvent _OnPizzaDelivered = default;
        [SerializeField] private IntEvent _OnPizzaDeliveredChange = default;
        [SerializeField] private IntEvent _OnPizzaDeliveredMaximumChange = default;

        [SerializeField] private GameEvent _OnGameOver = default;

        private void OnEnable() 
        {
            _OnCoinCollect.OnInt += _AddCoin;
            _OnAddPizza.OnInt += _AddPizza;
            _OnRemovePizza.OnInt += _RemovePizza;
            _OnPizzaDelivered.OnGameListener += _DeliveredPizza;
            _OnGameOver.OnGameListener += _GameOver;
        }

        private void OnDisable() 
        {
            _OnCoinCollect.OnInt -= _AddCoin;
            _OnAddPizza.OnInt -= _AddPizza;
            _OnRemovePizza.OnInt -= _RemovePizza;
            _OnPizzaDelivered.OnGameListener -= _DeliveredPizza;
            _OnGameOver.OnGameListener -= _GameOver;
        }

        private void Start() 
        {
            _OnCoinChange.Raise(_coin);
            _OnPizzaChange.Raise(_pizzas);
            _OnPizzaDeliveredChange.Raise(_pizzasDelivered);
        }

        private void _AddCoin(int amount)
        {
            _coin += amount;
            _OnCoinChange.Raise(_coin);
        }

        private void _AddPizza(int amount)
        {
            _pizzas += amount;
            _OnPizzaChange.Raise(_pizzas);
        }

        private void _RemovePizza(int amount)
        {
            _pizzas -= amount;

            if(_pizzas < 0)
            {
                _pizzas = 0;
            }

            _OnPizzaChange.Raise(_pizzas);
        }

        private void _AddPizzaDelivered(int amount)
        {
            _pizzasDelivered += amount;
            _OnPizzaDeliveredChange.Raise(_pizzasDelivered);
        }

        private void _DeliveredPizza()
        {
            if(_pizzas <= 0) return;

            _RemovePizza(1);
            _AddPizzaDelivered(1);
        }

        private void _GameOver()
        {
            _SaveGameStats();
        }

        private void _SaveGameStats()
        {
            _SaveCoin();
            _SaveHighPizzaDelivered();
        }

        private void _SaveCoin()
        {
            GameStatsData data = SaveGame.Load();
            data.coin += _coin;
            SaveGame.Save(data);
        }

        private void _SaveHighPizzaDelivered()
        {
            GameStatsData data = SaveGame.Load();

            if(_pizzasDelivered > data.pizzaDeliveredMaximum)
            {
                data.pizzaDeliveredMaximum = _pizzasDelivered;
            }

            _OnPizzaDeliveredMaximumChange.Raise(data.pizzaDeliveredMaximum);
            SaveGame.Save(data);
        }
    }
}