﻿using System.Collections;
using UnityEngine;
using EndlessRunner.Utils;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Manager
{
    public sealed class SceneManager : MonoBehaviour
    {
        private enum SceneGame { Menu = 0, Game = 1 };
        public static SceneManager instance;
        [SerializeField] private GameEvent onMenuToGame = default;
        [SerializeField] private GameEvent onRestartGame = default;
        [SerializeField] private GameEvent onBackToMenu = default;
        [SerializeField] private Fade fade;

        private void Awake() 
        {
            if(instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        private void OnEnable() 
        {
            onMenuToGame.OnGameListener += _ToGame;
            onRestartGame.OnGameListener += _RestartScene;
            onBackToMenu.OnGameListener += _ToMenu;
        }

        private void OnDisable() 
        {
            onMenuToGame.OnGameListener -= _ToGame;
            onRestartGame.OnGameListener -= _RestartScene;
            onBackToMenu.OnGameListener -= _ToMenu;
        }

        private void _ToGame()
        {
            StartCoroutine(_ChangeScene(SceneGame.Game));
        }

        private void _RestartScene()
        {
            StartCoroutine(_ChangeScene(SceneGame.Game));
        }

        private void _ToMenu()
        {
            StartCoroutine(_ChangeScene(SceneGame.Menu));
        }

        private IEnumerator _ChangeScene(SceneGame scene)
        {
            int index = (int)scene;
            yield return StartCoroutine(_LoadSceneCoroutine(index));
            Time.timeScale = 1;
        }

        private IEnumerator _LoadSceneCoroutine(int index)
        {
            yield return StartCoroutine(fade.FadeInCoroutine());
            AsyncOperation operation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(index);
            
            while(operation.isDone == false)
            {
                yield return null;
            }

            yield return StartCoroutine(fade.FadeOutCoroutine());
        }
    }
}
