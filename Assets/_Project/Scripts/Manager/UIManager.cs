﻿using UnityEngine;
using TMPro;

using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Manager
{
    public sealed class UIManager : MonoBehaviour
    {
        [SerializeField] private IntEvent _OnCoinChange = default;
        [SerializeField] private IntEvent _OnPizzaChange = default;
        [SerializeField] private IntEvent _OnPizzaDeliveredChange = default;
        [SerializeField] private TextMeshProUGUI _coinText;
        [SerializeField] private TextMeshProUGUI _pizzaText;
        [SerializeField] private TextMeshProUGUI _pizzaDeliveredText;

        private void OnEnable() 
        {
            _OnCoinChange.OnInt += _UpdateCoinText;
            _OnPizzaChange.OnInt += _UpdatePizzaText;
            _OnPizzaDeliveredChange.OnInt += _UpdatePizzaDeliveredText;
        }

        private void OnDisable() 
        {
            _OnCoinChange.OnInt -= _UpdateCoinText;
            _OnPizzaChange.OnInt -= _UpdatePizzaText;
            _OnPizzaDeliveredChange.OnInt -= _UpdatePizzaDeliveredText;
        }

        private void _UpdateCoinText(int total)
        {
            _coinText.text = total.ToString();
        }

        private void _UpdatePizzaText(int total)
        {
            _pizzaText.text = string.Concat("x", total.ToString());
        }

        private void _UpdatePizzaDeliveredText(int total)
        {
            _pizzaDeliveredText.text = string.Concat("Pizzas entregues: ", total);
        }
    }
}