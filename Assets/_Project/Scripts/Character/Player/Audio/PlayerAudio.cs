﻿using UnityEngine;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Character.Player.Audio
{
    public sealed class PlayerAudio : MonoBehaviour
    {
        [SerializeField] private GameEvent _OnPlayerIsJump = default;
        [SerializeField] private AudioEvent _OnAudioSfx = default;
        [SerializeField] private AudioClip _jumpSfx = default;

        private void OnEnable() 
        {
            _OnPlayerIsJump.OnGameListener += _Jump;
        }

        private void OnDisable() 
        {
            _OnPlayerIsJump.OnGameListener -= _Jump;
        }

        private void _Jump()
        {
            _OnAudioSfx.Raise(_jumpSfx);
        }
    }
}
