﻿using UnityEngine;

namespace EndlessRunner.Character.Player.Animation
{
    public sealed class PlayerAnimatorParameters
    {
        public static readonly string AnimatorParameterIsGround = "IsGround";
        public static readonly string AnimatorParameterJumpTrigger = "Jump";
        public static readonly string AnimatorParameterVelocityVertical = "VelocityVertical";
    }
}
