﻿using UnityEngine;
using EndlessRunner.Character.Player.Collision;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Character.Player.Animation
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PlayerCheckGround))]
    public sealed class PlayerAnimation : MonoBehaviour
    {
        private Rigidbody _rb;
        private PlayerCheckGround _checkGround;
        [SerializeField] private Animator _playerAnimator;
        [SerializeField] private GameEvent _OnPlayerIsJump = default;
        private bool _isGround = false;

        private void OnEnable() 
        {
            _OnPlayerIsJump.OnGameListener += _JumpTrigger;
        }

        private void OnDisable() 
        {
            _OnPlayerIsJump.OnGameListener -= _JumpTrigger;
        }

        private void Awake() 
        {
            _InitializeReferences();
        }

        private void Update()
        {
            _CheckGround();
            _playerAnimator.SetBool(PlayerAnimatorParameters.AnimatorParameterIsGround, _isGround);
            _playerAnimator.SetFloat(PlayerAnimatorParameters.AnimatorParameterVelocityVertical, _rb.velocity.y);
        }

        private void _InitializeReferences()
        {
            _rb = GetComponent<Rigidbody>();
            _checkGround = GetComponent<PlayerCheckGround>();
        }

        private void _CheckGround()
        {
            _isGround = _checkGround.isGround;
        }

        private void _JumpTrigger()
        {
            _playerAnimator.SetTrigger(PlayerAnimatorParameters.AnimatorParameterJumpTrigger);
        }
    }
}
