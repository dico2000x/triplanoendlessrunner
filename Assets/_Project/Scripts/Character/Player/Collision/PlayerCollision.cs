﻿using UnityEngine;
using EndlessRunner.GameOver;
using EndlessRunner.Interfaces.Collision;

namespace EndlessRunner.Character.Player.Collision
{
    public sealed class PlayerCollision : MonoBehaviour
    {
        private void _CheckGameOver(GameObject collisionImpact)
        {
            KillPlayer gameOver = collisionImpact.GetComponent<KillPlayer>();
            if(gameOver != null)
            {
                gameOver.ActiveGameOver();
            }
        }

        private void _CheckPickup(GameObject collisionImpact)
        {
            IPlayerTrigger pickup = collisionImpact.GetComponent<IPlayerTrigger>();
            if(pickup != null)
            {
                pickup.Interactable();
            }
        }

        private void OnCollisionEnter(UnityEngine.Collision other)
        {
            _CheckGameOver(other.gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            _CheckPickup(other.gameObject);
        }
    }
}
