﻿using UnityEngine;

namespace EndlessRunner.Character.Player.Collision
{
    public sealed class PlayerCheckGround : MonoBehaviour
    {
        public bool isGround = false;
        [SerializeField] private float _distanceRay = 1;
        [SerializeField] private Transform _checkGroundPoint;
        [SerializeField] private LayerMask _groundLayer;

        [Header("Gizmos Settings")]
        [SerializeField] private Color _rayColor = Color.red;

        private void FixedUpdate()
        {
            _CheckGround();
        }

        private void _CheckGround()
        {
            isGround = Physics.Raycast(_checkGroundPoint.position, Vector3.down, _distanceRay, _groundLayer);
        }

        private void OnDrawGizmos() 
        {
            if(_checkGroundPoint)
            {
                Gizmos.color = _rayColor;
                Gizmos.DrawRay(_checkGroundPoint.position, Vector3.down * _distanceRay);
            }    
        }
    }
}