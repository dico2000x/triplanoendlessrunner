﻿using UnityEngine;
using EndlessRunner.Utils;
using EndlessRunner.Character.Player.Collision;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Character.Player.Movement
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PlayerCheckGround))]
    public sealed class PlayerMovement : MonoBehaviour
    {
        private Rigidbody _rb;
        private LineMovement _line;
        private Transform _playerTransform;
        private PlayerCheckGround _checkGround;
        [SerializeField] private GameEvent _OnPlayerIsJump = default;
        [Header("Movement Settings")]
        [SerializeField] private int _lineStart = 1;
        [SerializeField] private float _moveSpeedForward = 0.3f;
        [SerializeField] private float _moveSpeedHorizontal = 0.7f;
        private Vector3 _targetPosition = Vector3.zero;

        [Header("Jump Settings")]
        [SerializeField] private float _jumpForce = 12;
        private bool _isGround = false;

        public void Initialize(float moveSpeedForward = 0.3f, float moveSpeedHorizontal = 0.7f, float jumpForce = 12)
        {
            _moveSpeedForward = moveSpeedForward;
            _moveSpeedHorizontal = moveSpeedHorizontal;
            _jumpForce = jumpForce;
            this.Awake();
        }

        public void MoveToLineLeft()
        {
            _line.MoveLeft();
        }

        public void MoveToLineRight()
        {
            _line.MoveRight();
        }

        public void Jump()
        {
            if(_isGround)
            {
                Vector3 velocity = _rb.velocity;
                velocity.y = _jumpForce;
                _rb.velocity = velocity;
                _OnPlayerIsJump.Raise();
            }
        }

        private void Awake()
        {
            _InitializeReferences();
        }

        private void Start() 
        {
            _SetTargetPosition();    
        }

        private void Update() 
        {
            _CheckGround();    
        }

        private void FixedUpdate()
        {
            _MoveForward();
            _MoveHorizontal();
        }

        private void _InitializeReferences()
        {
            _line = new LineMovement(_lineStart);
            _playerTransform = this.transform;
            _rb = GetComponent<Rigidbody>();
            _checkGround = GetComponent<PlayerCheckGround>();
        }

        private void _MoveForward()
        {
            _rb.MovePosition(_rb.position + Vector3.forward * _moveSpeedForward);
        }

        private void _MoveHorizontal()
        {            
            _SetTargetPosition();
            Vector3 move = Vector3.MoveTowards(_playerTransform.position, _targetPosition, _moveSpeedHorizontal);
            move.y = _rb.position.y;
            move.z = _rb.position.z;
            _rb.MovePosition(move);
        }

        private void _SetTargetPosition()
        {
            _targetPosition = _playerTransform.position;
            _targetPosition.x = _line.linePositionCurrent;
        }

        private void _CheckGround()
        {
            _isGround = _checkGround.isGround;
        }
    }
}
