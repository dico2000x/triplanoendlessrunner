﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace EndlessRunner.Character.Player.Input.Mobile
{
    public sealed class PlayerMobileInput : PlayerInput
    {
        private PlayerMobileInputAction _playerMobileAction;
        [SerializeField] private float _maximumTimeForValidateMovimentTouch = 0.5f;
        private Vector2 _startTouchPosition;
        private Vector2 _directionTouch;
        [SerializeField] private float _distanceTouchHorizontal = 100;
        [SerializeField] private float _distanceTouchVertical = 60;

        private void OnEnable()
        {
            _playerMobileAction.Enable();
        }

        private void OnDisable()
        {
            _playerMobileAction.Disable();
        }

        protected sealed override void Awake()
        {
            base.Awake();
            _playerMobileAction = new PlayerMobileInputAction();
        }

        private void Start() 
        {
            _InitializeTouch();
        }

        protected sealed override void Update()
        {
            base.Update();
            _directionTouch = Vector2.zero;
        }

        private void _InitializeTouch()
        {
            _playerMobileAction.Touch.TouchPress.started += ctx => _StartTouch(ctx);
            _playerMobileAction.Touch.TouchPress.canceled += ctx => _EndTouch(ctx);
        }

        protected sealed override bool _CanMoveLeft()
        {
            return _directionTouch.x <= -_distanceTouchHorizontal && Mathf.Abs(_directionTouch.x) > _directionTouch.y;
        }

        protected sealed override bool _CanMoveRight()
        {
            return _directionTouch.x >= _distanceTouchHorizontal && _directionTouch.x > _directionTouch.y;
        }

        protected sealed override bool _CanJump()
        {
            return _directionTouch.y >= _distanceTouchVertical && _directionTouch.y > _directionTouch.x;
        }

        private void _StartTouch(InputAction.CallbackContext context)
        {
            _startTouchPosition = _playerMobileAction.Touch.TouchPosition.ReadValue<Vector2>();
        }

        private void _EndTouch(InputAction.CallbackContext context)
        {
            bool canMove = _ValidateTimeTouch(context); 
            if(canMove)
            {
                Vector2 endTouchPosition = _playerMobileAction.Touch.TouchPosition.ReadValue<Vector2>();
                _directionTouch = endTouchPosition - _startTouchPosition;
            }
        }

        private bool _ValidateTimeTouch(InputAction.CallbackContext context)
        {
            double duration = context.time - context.startTime;
            return duration <= _maximumTimeForValidateMovimentTouch;
        }
    }
}
