﻿using UnityEngine;
using EndlessRunner.Character.Player.Movement;

namespace EndlessRunner.Character.Player.Input
{
    public abstract class PlayerInput : MonoBehaviour
    {
        protected abstract bool _CanMoveLeft();
        protected abstract bool _CanMoveRight();
        protected abstract bool _CanJump();
        private bool _canJump = false;
        private PlayerMovement _moviment;

        protected virtual void Awake()
        {
            _InitializeReferences();
        }

        protected virtual void Update()
        {
            _Move();

            if(_CanJump())
            {
                _canJump = true;
            }
        }

        private void FixedUpdate() 
        {
            _Jump();
        }

        private void _InitializeReferences()
        {
            _moviment = GetComponent<PlayerMovement>();
        }

        private void _Move()
        {
            if(_CanMoveLeft())
            {
                _moviment.MoveToLineLeft();
            }
            else if(_CanMoveRight())
            {
                _moviment.MoveToLineRight();
            }
        }

        private void _Jump()
        {
            if(_canJump)
            {
                _canJump = false;
                _moviment.Jump();
            }
        }
    }
}