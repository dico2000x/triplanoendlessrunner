﻿namespace EndlessRunner.Character.Player.Input.Keyboard
{
    public sealed class PlayerKeyboardInput : PlayerInput
    {
        private PlayerKeyboardInputAction _keyboardInputAction;

        private void OnEnable()
        {
            _keyboardInputAction.Enable();
        }

        private void OnDisable()
        {
            _keyboardInputAction.Disable();
        }

        protected sealed override void Awake()
        {
            base.Awake();
            _keyboardInputAction = new PlayerKeyboardInputAction();
        }

        protected sealed override bool _CanMoveLeft()
        {
            return _keyboardInputAction.PlayerActions.MoveLeft.triggered;
        }

        protected sealed override bool _CanMoveRight()
        {
            return _keyboardInputAction.PlayerActions.MoveRight.triggered;
        }

        protected sealed override bool _CanJump()
        {
            return _keyboardInputAction.PlayerActions.Jump.triggered;
        }
    }
}
