﻿using UnityEngine;
using EndlessRunner.Interfaces.Collision;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Character.Client
{
    public sealed class Client : MonoBehaviour , IPlayerTrigger
    {
        [SerializeField] private IntEvent _OnCoinCollect = default;
        [SerializeField] private IntEvent _OnPizzaChange = default;
        [SerializeField] private GameEvent _OnPizzaDelivered = default;
        [SerializeField] private int _coinMinimum = 20;
        [SerializeField] private int _coinMaximum = 50;
        private int _pizzas;

        private void OnEnable() 
        {
            _OnPizzaChange.OnInt += _SetPizzas;
        }

        private void OnDisable() 
        {
            _OnPizzaChange.OnInt -= _SetPizzas;
        }

        private void _SetPizzas(int total)
        {
            _pizzas = total;
        }

        void IPlayerTrigger.Interactable()
        {
            _DeliveredPizza();
        }

        private void _DeliveredPizza()
        {
            _PayPizza();
            _OnPizzaDelivered.Raise();
        }

        private void _PayPizza()
        {
            if(HasPizzas())
            {
                int coin = _GetRandomCoin();
                _OnCoinCollect.Raise(coin);
            }
        }

        private bool HasPizzas()
        {
            return _pizzas > 0;
        }

        private int _GetRandomCoin()
        {
            int coin = Random.Range(_coinMinimum, _coinMaximum);
            return coin;
        }
    }
}