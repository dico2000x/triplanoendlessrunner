﻿namespace EndlessRunner.Interfaces.Collision
{
    public interface IPlayerTrigger
    {
        void Interactable();
    }
}
