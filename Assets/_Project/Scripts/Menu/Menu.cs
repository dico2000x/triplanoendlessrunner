﻿using UnityEngine;
using UnityEngine.UI;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Menu
{
    public sealed class Menu : MonoBehaviour
    {
        [SerializeField] private GameEvent onMenuToGame = default;
        [SerializeField] private Button playGameButton;
        [SerializeField] private Button quitGameButton;

        private void Awake()
        {
            playGameButton.onClick.AddListener(_HandlePlayGame);
            quitGameButton.onClick.AddListener(_HandleQuitGame);
        }

        private void _HandlePlayGame()
        {
            onMenuToGame.Raise();
        }

        private void _HandleQuitGame()
        {
            Application.Quit();
        }
    }
}
