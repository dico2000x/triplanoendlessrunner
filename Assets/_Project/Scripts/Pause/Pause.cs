﻿using UnityEngine;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.Pause
{
    public sealed class Pause : MonoBehaviour
    {
        [SerializeField] private GameEvent _OnGamePaused = default;
        [SerializeField] private GameEvent _OnResumeGame = default;

        private void OnEnable() 
        {
            _OnGamePaused.OnGameListener += _PauseGame;
            _OnResumeGame.OnGameListener += _ResumeGame;
        }

        private void OnDisable() 
        {
            _OnGamePaused.OnGameListener -= _PauseGame;
            _OnResumeGame.OnGameListener -= _ResumeGame;
        }

        private void _PauseGame()
        {
            Time.timeScale = 0;
        }

        private void _ResumeGame()
        {
            Time.timeScale = 1;
        }
    }
}