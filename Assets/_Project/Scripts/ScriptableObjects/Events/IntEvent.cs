﻿using System;
using UnityEngine;

namespace EndlessRunner.ScriptableObjects.Events
{
    [CreateAssetMenu(fileName = "IntEvent", menuName = "ScriptableObjects/Events/IntEvent")]
    public sealed class IntEvent : ScriptableObject
    {
        public event Action<int> OnInt;

        public void Raise(int value)
        {
            OnInt?.Invoke(value);
        }
    }
}
