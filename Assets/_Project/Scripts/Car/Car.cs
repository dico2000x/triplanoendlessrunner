﻿using UnityEngine;

namespace EndlessRunner.Car
{
    public sealed class Car : MonoBehaviour 
    {
        [SerializeField] private float _moveSpeed = 10;
        [SerializeField] private float _distanceRay = 3;
        private bool _isCollisionInFront = false;
        [SerializeField] private Transform _checkPointCollider;
        [SerializeField] private LayerMask _colliderLayer;

        [Header("Gizmos settings")]
        [SerializeField] private Color _colliderRayColor = Color.red;

        private void Update()
        {
            if(_isCollisionInFront == false)
            {
                _MoveForward();
            }
        }

        private void FixedUpdate() 
        {
            _CheckForward();
        }

        private void _MoveForward()
        {
            transform.Translate(transform.forward * _moveSpeed * Time.deltaTime, Space.World);
        }

        private void _CheckForward()
        {
            _isCollisionInFront = Physics.Raycast(_checkPointCollider.position, transform.forward, _distanceRay, _colliderLayer);
        }

        private void OnDrawGizmos() 
        {
            if(_checkPointCollider)
            {
                Gizmos.color = _colliderRayColor;
                Gizmos.DrawRay(_checkPointCollider.position, transform.forward * _distanceRay);
            }
        }
    }
}