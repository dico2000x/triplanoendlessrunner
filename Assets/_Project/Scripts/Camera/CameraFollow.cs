﻿using UnityEngine;
using Cinemachine;

namespace EndlessRunner.Camera
{
    public sealed class CameraFollow : MonoBehaviour
    {
        private Transform _cameraTransform;
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;

        private void Awake() 
        {
            _cameraTransform = _virtualCamera.transform;
        }

        private void Update()
        {
            SetPosition();
        }

        private void SetPosition()
        {
            Vector3 forcePosition = new Vector3(0, _cameraTransform.position.y, _cameraTransform.position.z);
            _virtualCamera.ForceCameraPosition(forcePosition, _cameraTransform.rotation);
        }
    }
}