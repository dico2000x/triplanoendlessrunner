﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.UI.GameOver
{
    public sealed class UI_GameOver : MonoBehaviour
    {
        [SerializeField] private IntEvent _OnPizzaDeliveredChange = default;
        [SerializeField] private IntEvent _OnPizzaDeliveredMaximumChange = default;
        [SerializeField] private GameEvent _OnGameOver = default;
        [SerializeField] private GameEvent _OnRestartGame = default;
        [SerializeField] private GameEvent _OnBackToMenu = default;
        [SerializeField] private GameObject _panelGameOver;
        [SerializeField] private TextMeshProUGUI _pizzaDeliveredText;
        [SerializeField] private TextMeshProUGUI _maximumPizzaDeliveredText;
        [SerializeField] private Button _playAgainButton;
        [SerializeField] private Button _quitToMenuButton;
        private int _pizzaDelivered = 0;
        private int _pizzaDeliveredMaximum = 0;

        private void OnEnable() 
        {
            _OnGameOver.OnGameListener += _EndGame;
            _OnPizzaDeliveredChange.OnInt += _SetPizzaDelivered;
            _OnPizzaDeliveredMaximumChange.OnInt += _SetPizzaDeliveredMaximum;
        }

        private void OnDisable() 
        {
            _OnGameOver.OnGameListener -= _EndGame;
            _OnPizzaDeliveredChange.OnInt -= _SetPizzaDelivered;
            _OnPizzaDeliveredMaximumChange.OnInt -= _SetPizzaDeliveredMaximum;
        }

        private void Awake() 
        {
            _playAgainButton.onClick.AddListener(_HandlePlayAgain);
            _quitToMenuButton.onClick.AddListener(_HandleQuitToMenu);
        }

        private void _EndGame()
        {
            _panelGameOver.SetActive(true);
            _pizzaDeliveredText.text = string.Concat("Você entregou ", _pizzaDelivered, " pizzas");
        }

        private void _HandlePlayAgain()
        {
            _OnRestartGame.Raise();
        }

        private void _HandleQuitToMenu()
        {
            _OnBackToMenu.Raise();
        }

        private void _SetPizzaDelivered(int delivered)
        {
            _pizzaDelivered = delivered;
        }

        private void _SetPizzaDeliveredMaximum(int maximum)
        {
            _pizzaDeliveredMaximum = maximum;
            _maximumPizzaDeliveredText.text = string.Concat("Seu máximo foi ", _pizzaDeliveredMaximum, " pizzas");
        }
    }
}