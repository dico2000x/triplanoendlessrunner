﻿using UnityEngine;
using UnityEngine.UI;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.UI.Pause
{
    public sealed class UI_Pause : MonoBehaviour
    {
        [SerializeField] private GameEvent _OnGamePaused = default;
        [SerializeField] private GameEvent _OnResumeGame = default;
        [SerializeField] private GameObject _panelPause;
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Button _resumeButton;

        private void Awake()
        {
            _pauseButton.onClick.AddListener(_PauseGame);
            _resumeButton.onClick.AddListener(_ResumeGame);
        }

        private void _PauseGame() 
        {
            _panelPause.SetActive(true);
            _OnGamePaused.Raise();
        }

        private void _ResumeGame()
        {
            _panelPause.SetActive(false);
            _OnResumeGame.Raise();
        }
    }
}