﻿using UnityEngine;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.GameOver
{
    public sealed class KillPlayer : MonoBehaviour
    {
        [SerializeField] private GameEvent _OnGameOver = default;

        public void ActiveGameOver()
        {
            _OnGameOver.Raise();
        }
    }
}