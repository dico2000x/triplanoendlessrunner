﻿using UnityEngine;
using EndlessRunner.ScriptableObjects.Events;

namespace EndlessRunner.GameOver
{
    public sealed class GameOver : MonoBehaviour
    {   
        [SerializeField] private GameEvent _OnGameOver = default;

        private void OnEnable() 
        {
            _OnGameOver.OnGameListener += EndGame;
        }

        private void OnDisable() 
        {
            _OnGameOver.OnGameListener -= EndGame;
        }

        private void EndGame()
        {
            PauseGame();
        }

        private void PauseGame()
        {
            Time.timeScale = 0;
        }
    }
}